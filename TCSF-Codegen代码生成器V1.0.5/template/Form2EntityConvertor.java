package ${form2EntityConvertorPackage};

import ${entityClass};
import ${formClass};
import cn.topcodes.tcsf.admin.commons.convertor.Form2EntityConvertor;
import org.springframework.stereotype.Component;

/**
 * ${displayName} 表单->实体转换器
 * @author ${author}
 * @date ${date}
 */
@Component
public class ${form2EntityConvertorName} extends Form2EntityConvertor<${entityName},${formName}> {

}
