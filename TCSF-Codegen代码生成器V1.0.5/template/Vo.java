package ${voPackage};

import lombok.Data;
import java.util.Date;
import com.alibaba.fastjson.annotation.JSONField;

/**
 * ${displayName} VO
 * @author ${author}
 * @date ${date}
 */
@Data
public class ${voName} {
	
    /**
     * 主键
     */
	private Long id;

    /**
     * 创建时间
     */
	private Date createdTime;
	
    /**
     * 最后更新时间
     */
	private Date updatedTime;	
	
    /**
     * 数据状态
     */
	private String dataState;

    <#list fields as field>
	<#if field.displayField>
    /**
     * ${field.displayName}
     */
	<#if field.jdbcDataType == "Date">
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	</#if>
	private ${field.jdbcDataType} ${field.propertyName};	
	
	</#if>
	</#list> 

}