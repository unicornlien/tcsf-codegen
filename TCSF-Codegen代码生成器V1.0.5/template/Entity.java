package ${entityPackage};

import cn.topcodes.tcsf.admin.commons.domain.IdEntity;
import lombok.Data;
import javax.persistence.Table;
import java.util.Date;

/**
 * ${displayName}
 * @author ${author}
 * @date ${date}
 */
@Data
@Table(name="${tableName}")
public class ${entityName} extends IdEntity {
	
	<#list fields as field>
	public static final String FIELD_${field.fieldName?upper_case} = "${field.propertyName}";

	</#list> 
	
    <#list fields as field>
    /**
     * ${field.displayName}
     */
	private ${field.jdbcDataType} ${field.propertyName};		
	
	</#list> 

}