package ${formPackage};

import cn.topcodes.tcsf.admin.commons.domain.Idable;
import lombok.Data;
import java.util.Date;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * ${displayName}表单
 * @author ${author}
 * @date ${date}
 */
@Data
public class ${formName} implements Idable {
	
	private Long id;

    <#list fields as field>
	<#if field.formField>
	<#if !field.allowNull>
	@NotNull(message = "${field.displayName}不能为空")
	</#if>
	<#if field.jdbcDataType == "String">
	@Length(min = ${field.min},max = ${field.max},message = "${field.displayName}长度为${field.min}~${field.max}个字符")
	</#if>
	<#if field.jdbcDataType == "Date">
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	</#if>
	private ${field.jdbcDataType} ${field.propertyName};
	
	</#if>
	</#list> 

}