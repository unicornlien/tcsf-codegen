<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/layouts/taglib.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>${displayName}详情</title>
		<%@include file="/WEB-INF/layouts/detail-header.jsp"%>
	</head>
	<body>

		<div class="tpanel">
			<div class="panel-content">
				<div class="container-fluid">
			
					<form id="createForm" class="form-horizontal">
						<#list fields as field>  
						<#if field.displayField>
						<div class="form-group<#if field_index == 0> form-group-first</#if>">
							<label class="col-sm-2 control-label">${field.displayName}</label>
							<div class="col-sm-8">
								<label class="control-label detail-label">
								<#if field.jdbcDataType == "Date">
								<fmt:formatDate value="${r'${entity.'}${field.propertyName}}" pattern="${field.timeFormat}" />
								<#else>
								${r'${entity.'}${field.propertyName}}
								</#if>
								</label>
							</div>
						</div>
						</#if>
						</#list>  
					  	
					</form>
				
				</div>
			</div>
		</div>
	</body>
</html>
