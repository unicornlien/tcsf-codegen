/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cn.topcodes.tcsf.codegen.ui;

import cn.topcodes.tcsf.codegen.domain.entity.Field;
import cn.topcodes.tcsf.codegen.utils.ScreenUtil;
import cn.topcodes.tcsf.codegen.utils.SqliteOpenHelper;
import com.j256.ormlite.stmt.QueryBuilder;
import static com.sun.java.accessibility.util.AWTEventMonitor.addItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

/**
 *
 * @author 大爱阳哥
 */
public class FieldDetailDialog extends javax.swing.JDialog {
    
    private String fieldName;
    
    private Integer entityId;
    
    private String radioDateLimitValue = "选择不限";
    
    private String radioNumberType = "整数";

    /**
     * Creates new form FieldDetailDialog
     */
    public FieldDetailDialog(java.awt.Frame parent, boolean modal,String fieldName, Integer entityId) {
        super(parent, modal);
        initComponents();
        ScreenUtil.initLocation(this);
        
        ImageIcon imageIcon = new ImageIcon("resource/images/logo.png");  
        this.setIconImage(imageIcon.getImage());  
        
        this.panelDateTime.setVisible(false);
        this.panelNumber.setVisible(false);
        this.panelPassword.setVisible(false);
        this.panelText.setVisible(false);
        this.panelSelect.setVisible(false);
        
        
        this.fieldName = fieldName;
        this.entityId = entityId;
        this.setTitle(fieldName + " - 详细设置");
        this.lblFieldName.setText(fieldName);
        
        try {
            QueryBuilder qb = SqliteOpenHelper.getFieldDao().queryBuilder();
            qb.where().eq("field_name", fieldName).and().eq("entity_id", this.entityId);
            Field field = (Field) qb.queryForFirst();
            this.lblDataType.setText(field.getJdbcDataType());
            this.cbInputType.removeAllItems();
            this.cbInputType.addItemListener(new ItemListener() {

                @Override
                public void itemStateChanged(ItemEvent e) {
                    if(e.getStateChange() == ItemEvent.SELECTED) {
                        String value = (String) e.getItem();
                        panelNumber.setVisible(false);
                        panelDateTime.setVisible(false);
                        panelSelect.setVisible(false);
                        panelText.setVisible(false);
                        panelPassword.setVisible(false);
                        switch(value) {
                            case "日期":
                                panelDateTime.setVisible(true);
                                break;
                            case "数值":
                                panelNumber.setVisible(true);
                                break;
                            case "选择":
                                panelSelect.setVisible(true);
                                break;
                            case "文件上传":
                                break;
                            case "文本":
                                panelText.setVisible(true);
                                break;
                            case "密码":
                                panelPassword.setVisible(true);
                                break;
                            case "富文本":
                                break;
                        }
                    }
                }
                
            });
            if("Date".equals(field.getJdbcDataType())) {
                this.cbInputType.addItem("日期");
            }else if("Integer".equals(field.getJdbcDataType()) ||
                    "Long".equals(field.getJdbcDataType()) ||
                    "Float".equals(field.getJdbcDataType()) ||
                    "Double".equals(field.getJdbcDataType())) {
                this.cbInputType.addItem("数值");
                this.cbInputType.addItem("选择");
                if("Integer".equals(field.getJdbcDataType()) ||
                    "Long".equals(field.getJdbcDataType())) {
                    this.cbInputType.addItem("文件上传");
                }
                this.panelNumber.setVisible(true);
            }else if("String".equals(field.getJdbcDataType())) {
                this.cbInputType.addItem("密码");
                this.cbInputType.addItem("文本");
                this.cbInputType.addItem("选择");
                this.cbInputType.addItem("富文本");
                this.cbInputType.addItem("文件上传");
            }else if("Boolean".equals(field.getJdbcDataType())) {
                this.cbInputType.addItem("选择");
                this.panelSelect.setVisible(true);
            }else if("byte[]".equals(field.getJdbcDataType())) {
                this.cbInputType.addItem("富文本");
            }
            
            loadData(field);
        }catch(Exception e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    private void loadData(Field field) {
        this.cbInputType.setSelectedItem(field.getInputType());
        switch(field.getInputType()) {
            case "日期":
            {
                this.cbDateReadonly.setSelected(field.isReadonly());
                this.cbDateCurrent.setSelected(field.isCurrentDate());
                switch(field.getTimeLimit()) {
                    case "仅选择未来时间":
                        this.rbDateFuture.setSelected(true);
                        break;
                    case "仅选择过去时间":
                        this.rbDatePast.setSelected(true);
                        break;
                    case "选择不限":
                        this.rbDateUnlimited.setSelected(true);
                        break;
                }
                this.cbDateFormat.setSelectedItem(field.getTimeFormat());
                this.txtDateDefaultTime.setText(field.getDefaultValue());
                break;
            }
            case "密码":
            {
                this.cbPwdPlain.setSelected(field.isPlainPassword());
                this.cbPwdEncrypt.setSelected(field.isEncryptPassword());
                this.cbPwdConfirm.setSelected(field.isConfirmPassword());
                this.cbPwdStrength.setSelectedItem(field.getSubInputType());
                this.txtPwdMin.setValue(field.getMin());
                this.txtPwdMax.setValue(field.getMax());
                break;
            }
            case "文本":
            {
                this.cbTxtReadonly.setSelected(field.isReadonly());
                this.cbTxtType.setSelectedItem(field.getSubInputType());
                this.txtTxtRegExp.setText(field.getRegExp());
                this.txtTxtMin.setValue(field.getMin());
                this.txtTextMax.setValue(field.getMax());
                this.txtTxtDefault.setText(field.getDefaultValue());
                break;
            }
            case "选择":
            {
                this.cbSelectReadonly.setSelected(field.isReadonly());
                this.cbSelectType.setSelectedItem(field.getSubInputType());
                this.txtSelectItem.setText(field.getSelectItems());
                break;
            }
            case "数值":
            {
                this.cbNumberReadonly.setSelected(field.isReadonly());
                switch(field.getSubInputType()) {
                    case "整数":
                        this.rbNumberInt.setSelected(true);
                        break;
                    case "小数":
                        this.rbNumberFloat.setSelected(true);
                        break;
                }
                this.txtNumberMin.setValue(field.getMin());
                this.txtNumberMax.setValue(field.getMax());
                this.txtNumberDefault.setValue(Double.parseDouble(field.getDefaultValue()));
                break;
            }
            case "富文本":
                break;
            case "文件上传":
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        lblFieldName = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cbInputType = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        lblDataType = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        panelSelect = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtSelectItem = new javax.swing.JTextPane();
        cbSelectType = new javax.swing.JComboBox();
        jLabel17 = new javax.swing.JLabel();
        cbSelectReadonly = new javax.swing.JCheckBox();
        jLabel18 = new javax.swing.JLabel();
        panelNumber = new javax.swing.JPanel();
        rbNumberInt = new javax.swing.JRadioButton();
        rbNumberFloat = new javax.swing.JRadioButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtNumberMin = new javax.swing.JSpinner();
        txtNumberMax = new javax.swing.JSpinner();
        jLabel11 = new javax.swing.JLabel();
        txtNumberDefault = new javax.swing.JSpinner();
        cbNumberReadonly = new javax.swing.JCheckBox();
        panelDateTime = new javax.swing.JPanel();
        cbDateFormat = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        cbDateCurrent = new javax.swing.JCheckBox();
        rbDateFuture = new javax.swing.JRadioButton();
        rbDatePast = new javax.swing.JRadioButton();
        rbDateUnlimited = new javax.swing.JRadioButton();
        jLabel10 = new javax.swing.JLabel();
        txtDateDefaultTime = new javax.swing.JFormattedTextField();
        cbDateReadonly = new javax.swing.JCheckBox();
        panelPassword = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        cbPwdPlain = new javax.swing.JCheckBox();
        cbPwdConfirm = new javax.swing.JCheckBox();
        jLabel5 = new javax.swing.JLabel();
        txtPwdMin = new javax.swing.JSpinner();
        txtPwdMax = new javax.swing.JSpinner();
        cbPwdEncrypt = new javax.swing.JCheckBox();
        cbPwdStrength = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        panelText = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        cbTxtType = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        txtTxtRegExp = new javax.swing.JTextField();
        txtTxtMin = new javax.swing.JSpinner();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtTextMax = new javax.swing.JSpinner();
        jLabel16 = new javax.swing.JLabel();
        txtTxtDefault = new javax.swing.JTextField();
        cbTxtReadonly = new javax.swing.JCheckBox();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("字段名称：");

        lblFieldName.setText("无");

        jLabel2.setText("输入类型：");

        cbInputType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "密码", "文本", "日期", "数值", "选择" }));

        jLabel3.setText("数据类型：");

        lblDataType.setText("无");

        panelSelect.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        txtSelectItem.setText("Male=男,common\nFemale=女,common"); // NOI18N
        jScrollPane1.setViewportView(txtSelectItem);

        cbSelectType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "单选框", "复选框", "下拉单选框", "多选列表", "组合框" }));

        jLabel17.setText("类型：");

        cbSelectReadonly.setText("只读");

        jLabel18.setText("选项：");

        javax.swing.GroupLayout panelSelectLayout = new javax.swing.GroupLayout(panelSelect);
        panelSelect.setLayout(panelSelectLayout);
        panelSelectLayout.setHorizontalGroup(
            panelSelectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSelectLayout.createSequentialGroup()
                .addComponent(cbSelectReadonly, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelSelectLayout.createSequentialGroup()
                .addGroup(panelSelectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(jLabel18))
                .addGap(38, 38, 38)
                .addGroup(panelSelectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cbSelectType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panelSelectLayout.setVerticalGroup(
            panelSelectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelSelectLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbSelectReadonly)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(panelSelectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbSelectType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addGap(18, 18, 18)
                .addGroup(panelSelectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addContainerGap())
        );

        panelNumber.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        buttonGroup2.add(rbNumberInt);
        rbNumberInt.setSelected(true);
        rbNumberInt.setText("整数");
        rbNumberInt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbNumberIntActionPerformed(evt);
            }
        });

        buttonGroup2.add(rbNumberFloat);
        rbNumberFloat.setText("小数");
        rbNumberFloat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbNumberFloatActionPerformed(evt);
            }
        });

        jLabel8.setText("最大值：");

        jLabel9.setText("最小值：");

        jLabel11.setText("默认值：");

        cbNumberReadonly.setText("只读");

        javax.swing.GroupLayout panelNumberLayout = new javax.swing.GroupLayout(panelNumber);
        panelNumber.setLayout(panelNumberLayout);
        panelNumberLayout.setHorizontalGroup(
            panelNumberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNumberLayout.createSequentialGroup()
                .addGroup(panelNumberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelNumberLayout.createSequentialGroup()
                        .addGroup(panelNumberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelNumberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel8)
                                .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE))
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panelNumberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtNumberMax, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
                            .addComponent(txtNumberMin, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNumberDefault)))
                    .addGroup(panelNumberLayout.createSequentialGroup()
                        .addComponent(rbNumberInt, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(rbNumberFloat))
                    .addComponent(cbNumberReadonly, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panelNumberLayout.setVerticalGroup(
            panelNumberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNumberLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbNumberReadonly)
                .addGap(10, 10, 10)
                .addGroup(panelNumberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbNumberInt)
                    .addComponent(rbNumberFloat))
                .addGap(9, 9, 9)
                .addGroup(panelNumberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtNumberMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelNumberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtNumberMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelNumberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNumberDefault, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        panelDateTime.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        cbDateFormat.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "HH:mm:ss", "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm" }));

        jLabel7.setText("格式：");

        cbDateCurrent.setText("默认以当前时间");

        buttonGroup1.add(rbDateFuture);
        rbDateFuture.setText("仅选择未来时间");
        rbDateFuture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbDateFutureActionPerformed(evt);
            }
        });

        buttonGroup1.add(rbDatePast);
        rbDatePast.setText("仅选择过去时间");
        rbDatePast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbDatePastActionPerformed(evt);
            }
        });

        buttonGroup1.add(rbDateUnlimited);
        rbDateUnlimited.setSelected(true);
        rbDateUnlimited.setText("选择不限");
        rbDateUnlimited.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbDateUnlimitedActionPerformed(evt);
            }
        });

        jLabel10.setText("默认时间：");

        cbDateReadonly.setText("只读");

        javax.swing.GroupLayout panelDateTimeLayout = new javax.swing.GroupLayout(panelDateTime);
        panelDateTime.setLayout(panelDateTimeLayout);
        panelDateTimeLayout.setHorizontalGroup(
            panelDateTimeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDateTimeLayout.createSequentialGroup()
                .addGroup(panelDateTimeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbDateUnlimited)
                    .addGroup(panelDateTimeLayout.createSequentialGroup()
                        .addComponent(rbDateFuture)
                        .addGap(28, 28, 28)
                        .addComponent(rbDatePast)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelDateTimeLayout.createSequentialGroup()
                .addGroup(panelDateTimeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDateTimeLayout.createSequentialGroup()
                        .addComponent(cbDateReadonly)
                        .addGap(18, 18, 18)
                        .addComponent(cbDateCurrent))
                    .addGroup(panelDateTimeLayout.createSequentialGroup()
                        .addGroup(panelDateTimeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelDateTimeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cbDateFormat, 0, 221, Short.MAX_VALUE)
                            .addComponent(txtDateDefaultTime))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panelDateTimeLayout.setVerticalGroup(
            panelDateTimeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDateTimeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDateTimeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbDateReadonly)
                    .addComponent(cbDateCurrent))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addGroup(panelDateTimeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbDatePast, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rbDateFuture, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addComponent(rbDateUnlimited)
                .addGap(15, 15, 15)
                .addGroup(panelDateTimeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(cbDateFormat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelDateTimeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtDateDefaultTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        panelDateTimeLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cbDateFormat, jLabel7});

        panelDateTimeLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel10, txtDateDefaultTime});

        panelPassword.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        jLabel4.setText("最小长度：");

        cbPwdPlain.setText("明文输入");
        cbPwdPlain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbPwdPlainActionPerformed(evt);
            }
        });

        cbPwdConfirm.setText("生成确认密码");

        jLabel5.setText("最大长度：");

        cbPwdEncrypt.setText("加密存储");
        cbPwdEncrypt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbPwdEncryptActionPerformed(evt);
            }
        });

        cbPwdStrength.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "不限制", "纯数字", "纯字母", "字母+数字", "大写字母+小写字母+数字", "字母+数字+符号", "大写字母+小写字母+数字+符号" }));

        jLabel6.setText("强度：");

        javax.swing.GroupLayout panelPasswordLayout = new javax.swing.GroupLayout(panelPassword);
        panelPassword.setLayout(panelPasswordLayout);
        panelPasswordLayout.setHorizontalGroup(
            panelPasswordLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPasswordLayout.createSequentialGroup()
                .addComponent(cbPwdPlain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(cbPwdEncrypt)
                .addGap(24, 24, 24)
                .addComponent(cbPwdConfirm))
            .addGroup(panelPasswordLayout.createSequentialGroup()
                .addGroup(panelPasswordLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(panelPasswordLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbPwdStrength, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtPwdMin)
                    .addComponent(txtPwdMax, javax.swing.GroupLayout.Alignment.TRAILING)))
        );
        panelPasswordLayout.setVerticalGroup(
            panelPasswordLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPasswordLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(panelPasswordLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbPwdConfirm)
                    .addComponent(cbPwdEncrypt)
                    .addComponent(cbPwdPlain))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addGroup(panelPasswordLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbPwdStrength, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(panelPasswordLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtPwdMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelPasswordLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtPwdMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        panelPasswordLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cbPwdStrength, jLabel6});

        panelPasswordLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel4, txtPwdMin});

        panelPasswordLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel5, txtPwdMax});

        panelPasswordLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cbPwdConfirm, cbPwdEncrypt, cbPwdPlain});

        panelText.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        jLabel12.setText("类型：");

        cbTxtType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "邮箱", "身份证", "手机", "普通文本" }));

        jLabel13.setText("正则：");

        jLabel14.setText("最小长度：");

        jLabel15.setText("最大长度：");

        jLabel16.setText("默认值：");

        cbTxtReadonly.setText("只读");

        javax.swing.GroupLayout panelTextLayout = new javax.swing.GroupLayout(panelText);
        panelText.setLayout(panelTextLayout);
        panelTextLayout.setHorizontalGroup(
            panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTextLayout.createSequentialGroup()
                .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelTextLayout.createSequentialGroup()
                        .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTxtMin)
                            .addComponent(txtTextMax)
                            .addComponent(txtTxtDefault)
                            .addComponent(txtTxtRegExp)))
                    .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbTxtReadonly, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(panelTextLayout.createSequentialGroup()
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(cbTxtType, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18))
        );
        panelTextLayout.setVerticalGroup(
            panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelTextLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbTxtReadonly)
                .addGap(6, 6, 6)
                .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(cbTxtType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txtTxtRegExp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtTxtMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtTextMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addGap(14, 14, 14)
                .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTxtDefault, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)))
        );

        panelTextLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel16, txtTxtDefault});

        panelTextLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel15, txtTextMax});

        panelTextLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel14, txtTxtMin});

        panelTextLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel13, txtTxtRegExp});

        panelTextLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cbTxtType, jLabel12});

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelNumber, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(panelPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelDateTime, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelSelect, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(panelSelect, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelDateTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panelNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButton1.setText("保存");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("关闭");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addGap(22, 22, 22)
                        .addComponent(jButton2))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addComponent(jLabel1)
                                .addGap(6, 6, 6)
                                .addComponent(lblFieldName, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addComponent(jLabel3)
                                .addGap(6, 6, 6)
                                .addComponent(lblDataType, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addComponent(jLabel2)
                                .addGap(8, 8, 8)
                                .addComponent(cbInputType, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 21, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(lblFieldName))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(lblDataType))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel2))
                    .addComponent(cbInputType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbPwdEncryptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbPwdEncryptActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbPwdEncryptActionPerformed

    private void cbPwdPlainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbPwdPlainActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbPwdPlainActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            QueryBuilder qb = SqliteOpenHelper.getFieldDao().queryBuilder();
            qb.where().eq("field_name", fieldName).and().eq("entity_id", this.entityId);
            Field field = (Field) qb.queryForFirst();

            String inputType = this.cbInputType.getSelectedItem().toString();
            field.setInputType(inputType);
            switch(inputType) {
                case "日期":
                {
                    boolean isReadonly = this.cbDateReadonly.isSelected();
                    boolean isDefaultCurrentTime = this.cbDateCurrent.isSelected();
                    String limit = this.radioDateLimitValue;
                    String timeFormat = this.cbDateFormat.getSelectedItem().toString();
                    String defaultTime = this.txtDateDefaultTime.getText();
                    field.setReadonly(isReadonly);
                    field.setCurrentDate(isDefaultCurrentTime);
                    field.setTimeFormat(timeFormat);
                    field.setTimeLimit(limit);
                    field.setDefaultValue(defaultTime);
                    break;
                }
                case "密码":
                {
                    boolean isPlainPassword = this.cbPwdPlain.isSelected();
                    boolean isEncryptPassword = this.cbPwdEncrypt.isSelected();
                    boolean isConfirmPassword = this.cbPwdConfirm.isSelected();
                    field.setPlainPassword(isPlainPassword);
                    field.setEncryptPassword(isEncryptPassword);
                    field.setConfirmPassword(isConfirmPassword);
                    field.setSubInputType(this.cbPwdStrength.getSelectedItem().toString());
                    field.setMin(Double.parseDouble(this.txtPwdMin.getValue().toString()));
                    field.setMax(Double.parseDouble(this.txtPwdMax.getValue().toString()));
                    break;
                }
                case "文本":
                {
                    boolean isReadonly = this.cbTxtReadonly.isSelected();
                    String subInputType = this.cbTxtType.getSelectedItem().toString();
                    String regExp = this.txtTxtRegExp.getText();
                    field.setReadonly(isReadonly);
                    field.setSubInputType(subInputType);
                    field.setRegExp(regExp);
                    field.setMin(Double.parseDouble(this.txtTxtMin.getValue().toString()));
                    field.setMax(Double.parseDouble(this.txtTextMax.getValue().toString()));
                    field.setDefaultValue(this.txtTxtDefault.getText());
                    break;
                }
                case "选择":
                {
                    boolean isReadonly = this.cbSelectReadonly.isSelected();
                    String subInputType = this.cbSelectType.getSelectedItem().toString();
                    field.setReadonly(isReadonly);
                    field.setSubInputType(subInputType);
                    field.setSelectItems(this.txtSelectItem.getText());
                    break;
                }
                case "数值":
                {
                    boolean isReadonly = this.cbNumberReadonly.isSelected();
                    String subInputType = this.radioNumberType;
                    field.setReadonly(isReadonly);
                    field.setSubInputType(subInputType);
                    field.setMin(Double.parseDouble(this.txtNumberMin.getValue().toString()));
                    field.setMax(Double.parseDouble(this.txtNumberMax.getValue().toString()));
                    field.setDefaultValue(this.txtNumberDefault.getValue()+"");
                    break;
                }
                case "富文本":
                    break;
                case "文件上传":
                    break;
            }
            SqliteOpenHelper.getFieldDao().update(field);
            this.dispose();
        }catch(Exception e) {
            e.printStackTrace();
        }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void rbDateFutureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbDateFutureActionPerformed
        JRadioButton temp = (JRadioButton) evt.getSource();
        if(temp.isSelected()){
            this.radioDateLimitValue = temp.getText();
            System.out.println(temp.getText());
        }
    }//GEN-LAST:event_rbDateFutureActionPerformed

    private void rbDatePastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbDatePastActionPerformed
        JRadioButton temp = (JRadioButton) evt.getSource();
        if(temp.isSelected()){
            this.radioDateLimitValue = temp.getText();
            System.out.println(temp.getText());
        }
    }//GEN-LAST:event_rbDatePastActionPerformed

    private void rbDateUnlimitedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbDateUnlimitedActionPerformed
        JRadioButton temp = (JRadioButton) evt.getSource();
        if(temp.isSelected()){
            this.radioDateLimitValue = temp.getText();
        }
    }//GEN-LAST:event_rbDateUnlimitedActionPerformed

    private void rbNumberIntActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbNumberIntActionPerformed
        JRadioButton temp = (JRadioButton) evt.getSource();
        if(temp.isSelected()){
            this.radioNumberType = temp.getText();
        }
    }//GEN-LAST:event_rbNumberIntActionPerformed

    private void rbNumberFloatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbNumberFloatActionPerformed
        JRadioButton temp = (JRadioButton) evt.getSource();
        if(temp.isSelected()){
            this.radioNumberType = temp.getText();
        }
    }//GEN-LAST:event_rbNumberFloatActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JCheckBox cbDateCurrent;
    private javax.swing.JComboBox cbDateFormat;
    private javax.swing.JCheckBox cbDateReadonly;
    private javax.swing.JComboBox cbInputType;
    private javax.swing.JCheckBox cbNumberReadonly;
    private javax.swing.JCheckBox cbPwdConfirm;
    private javax.swing.JCheckBox cbPwdEncrypt;
    private javax.swing.JCheckBox cbPwdPlain;
    private javax.swing.JComboBox cbPwdStrength;
    private javax.swing.JCheckBox cbSelectReadonly;
    private javax.swing.JComboBox cbSelectType;
    private javax.swing.JCheckBox cbTxtReadonly;
    private javax.swing.JComboBox cbTxtType;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDataType;
    private javax.swing.JLabel lblFieldName;
    private javax.swing.JPanel panelDateTime;
    private javax.swing.JPanel panelNumber;
    private javax.swing.JPanel panelPassword;
    private javax.swing.JPanel panelSelect;
    private javax.swing.JPanel panelText;
    private javax.swing.JRadioButton rbDateFuture;
    private javax.swing.JRadioButton rbDatePast;
    private javax.swing.JRadioButton rbDateUnlimited;
    private javax.swing.JRadioButton rbNumberFloat;
    private javax.swing.JRadioButton rbNumberInt;
    private javax.swing.JFormattedTextField txtDateDefaultTime;
    private javax.swing.JSpinner txtNumberDefault;
    private javax.swing.JSpinner txtNumberMax;
    private javax.swing.JSpinner txtNumberMin;
    private javax.swing.JSpinner txtPwdMax;
    private javax.swing.JSpinner txtPwdMin;
    private javax.swing.JTextPane txtSelectItem;
    private javax.swing.JSpinner txtTextMax;
    private javax.swing.JTextField txtTxtDefault;
    private javax.swing.JSpinner txtTxtMin;
    private javax.swing.JTextField txtTxtRegExp;
    // End of variables declaration//GEN-END:variables
}
