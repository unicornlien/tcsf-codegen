package cn.topcodes.tcsf.codegen.ui;

import cn.topcodes.tcsf.codegen.domain.entity.Program;
import cn.topcodes.tcsf.codegen.utils.JdbcUtil;
import cn.topcodes.tcsf.codegen.utils.ScreenUtil;
import cn.topcodes.tcsf.codegen.utils.SqliteOpenHelper;
import com.mysql.jdbc.StringUtils;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.SQLException;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 *
 * @author Unicorn
 */
public class EditProgramDialog extends javax.swing.JDialog {
    
//    项目管理对话框
    private ProgramDialog programDialog;
    
//    当前项目ID
    private Integer programId;

    public static final int RET_CANCEL = 0;

    public static final int RET_OK = 1;

    public EditProgramDialog(java.awt.Frame parent, boolean modal, Integer programId, ProgramDialog programDialog) {
        super(parent, modal);
        initComponents();
        ScreenUtil.initLocation(this);
        
        ImageIcon imageIcon = new ImageIcon("resource/images/logo.png");  
        this.setIconImage(imageIcon.getImage());  

        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                doClose(RET_CANCEL);
            }
        });
        
        this.programDialog = programDialog;
        this.programId = programId;
        if(this.programId != null) {
            try {
                loadData();
            }catch(Exception e) {
                JOptionPane.showMessageDialog(this, "加载项目信息失败");
            }
            
        }
    }
    
//    加载项目数据
    private void loadData() throws SQLException {
        Program program = (Program)SqliteOpenHelper.getProgramDao().queryForId(this.programId);
        if(program == null) {
            JOptionPane.showMessageDialog(this, "加载项目信息失败");
            return;
        }
        this.txtProgramName.setText(program.getName());
        this.txtPackage.setText(program.getPackageName());
        this.txtAuthor.setText(program.getAuthor());
        this.txtFolderPath.setText(program.getFolderPath());
        this.cbDbType.setSelectedItem(program.getDbType());
        this.txtIp.setText(program.getIp());
        this.txtPort.setText(program.getPort() + "");
        this.txtUsername.setText(program.getUsername());
        this.txtPassword.setText(program.getPassword());
        this.txtPrefix.setText(program.getPrefix());
        
        List<String> databases = JdbcUtil.getDatabases(program.getDbType(), program.getIp(), program.getPort(),
                program.getUsername(), program.getPassword());
        if(databases == null) {
            JOptionPane.showMessageDialog(this, "连接失败");
        }else if(databases.isEmpty()) {
            
        }else {
            for(String database : databases) {
                this.cbDbName.addItem(database);
            }
            this.cbDbName.setSelectedItem(program.getDbName());
            this.cbDbName.setEnabled(true);
            this.btnCreate.setEnabled(true);
        }
    }

    public int getReturnStatus() {
        return returnStatus;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fcGenPathChooser = new javax.swing.JFileChooser();
        btnCreate = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtUsername = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cbDbType = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        txtProgramName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cbDbName = new javax.swing.JComboBox();
        btnConnect = new javax.swing.JButton();
        txtPassword = new javax.swing.JPasswordField();
        jLabel7 = new javax.swing.JLabel();
        txtPackage = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtIp = new javax.swing.JTextField();
        txtPort = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtFolderPath = new javax.swing.JTextField();
        btnChoseFolder = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        txtAuthor = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtPrefix = new javax.swing.JTextField();

        fcGenPathChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        setTitle("编辑项目信息");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        btnCreate.setText("保存");
        btnCreate.setEnabled(false);
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        btnCancel.setText("取消");
        btnCancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnCancelMousePressed(evt);
            }
        });
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        jLabel1.setText("用户名：");

        txtUsername.setText("root");

        jLabel2.setText("密码：");

        jLabel3.setText("类型：");

        cbDbType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "MySQL", "Oracle", "SqlServer" }));

        jLabel5.setText("项目名称：");

        txtProgramName.setText("项目名称");

        jLabel4.setText("请填写以下项目信息");

        jLabel6.setText("数据库：");

        cbDbName.setEnabled(false);

        btnConnect.setText("连接");
        btnConnect.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnConnectMousePressed(evt);
            }
        });

        jLabel7.setText("包名：");

        txtPackage.setText("cn.topcodes.demo");
        txtPackage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPackageActionPerformed(evt);
            }
        });

        jLabel8.setText("IP：");

        jLabel9.setText("端口：");

        txtIp.setText("127.0.0.1");

        txtPort.setText("3306");

        jLabel10.setText("生成位置：");

        btnChoseFolder.setText("...");
        btnChoseFolder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChoseFolderActionPerformed(evt);
            }
        });

        jLabel11.setText("作者：");

        txtAuthor.setText("codegen");

        jLabel12.setText("去除前缀：");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnCancel)
                .addGap(22, 22, 22))
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel6)
                            .addComponent(jLabel9)
                            .addComponent(jLabel8)
                            .addComponent(jLabel3)
                            .addComponent(jLabel7)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12))
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cbDbName, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnConnect, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(txtProgramName, javax.swing.GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
                            .addComponent(cbDbType, 0, 213, Short.MAX_VALUE)
                            .addComponent(txtIp, javax.swing.GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
                            .addComponent(txtPort, javax.swing.GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
                            .addComponent(txtPackage, javax.swing.GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
                            .addComponent(txtUsername, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
                            .addComponent(txtPassword, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(txtFolderPath, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnChoseFolder, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                            .addComponent(txtAuthor)
                            .addComponent(txtPrefix))))
                .addContainerGap(73, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnCancel, btnCreate});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel3, jLabel5, jLabel6, jLabel7, jLabel8, jLabel9});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cbDbType, txtIp, txtPackage, txtPassword, txtPort, txtProgramName, txtUsername});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtProgramName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(txtPackage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(txtAuthor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtFolderPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChoseFolder))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(txtPrefix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbDbType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtIp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbDbName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnConnect)))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel)
                    .addComponent(btnCreate))
                .addGap(19, 19, 19))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnChoseFolder, cbDbName, cbDbType, jLabel1, jLabel12, jLabel2, jLabel3, jLabel5, jLabel6, jLabel7, jLabel8, jLabel9, txtFolderPath, txtIp, txtPackage, txtPassword, txtPort, txtPrefix, txtUsername});

        getRootPane().setDefaultButton(btnCreate);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        try {
            if(checkInput()) {
                Program program = new Program();
                program.setName(this.txtProgramName.getText());
                program.setPackageName(this.txtPackage.getText());
                program.setAuthor(this.txtAuthor.getText());
                program.setFolderPath(this.txtFolderPath.getText());
                program.setPrefix(this.txtPrefix.getText());
                program.setDbType(this.cbDbType.getSelectedItem().toString());
                program.setIp(this.txtIp.getText());
                program.setPort(Integer.parseInt(this.txtPort.getText()));
                program.setUsername(this.txtUsername.getText());
                program.setPassword(this.txtPassword.getText());
                program.setDbName(this.cbDbName.getSelectedItem().toString());
                //  创建项目
                if(this.programId == null) {
                    SqliteOpenHelper.getProgramDao().create(program);
                }else {
                    if(MainFrame.isInit() && MainFrame.getInstance().getProgramId() == this.programId) {
                        MainFrame.getInstance().clear();
                    }
                    //  修改项目
                    program.setId(this.programId);
                    SqliteOpenHelper.getProgramDao().update(program);
                }
                if(this.programDialog != null) {
                    //  项目管理对话框重新加载数据
                    this.programDialog.loadData();
                }
                this.dispose();
            }else {
                this.cbDbName.setEnabled(false);
                this.btnCreate.setEnabled(false);
            }
        }catch(Exception e) {
            JOptionPane.showMessageDialog(this, "写入数据库失败");
        }
    }//GEN-LAST:event_btnCreateActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        doClose(RET_CANCEL);
    }//GEN-LAST:event_btnCancelActionPerformed

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        doClose(RET_CANCEL);
    }//GEN-LAST:event_closeDialog

    //  检查输入
    private boolean checkInput() {
        String programName = this.txtProgramName.getText();
        if(programName.length() == 0 || programName.length() > 10) {
            JOptionPane.showMessageDialog(this, "项目名称长度为1~10个字符");
            return false;
        }
        String packageName = this.txtPackage.getText();
        if(packageName.length() == 0) {
            JOptionPane.showMessageDialog(this, "请输入包名");
            return false;
        }
        String author = this.txtAuthor.getText();
        if(author.length() == 0) {
            JOptionPane.showMessageDialog(this, "请输入作者");
            return false;
        }
        String folderPath = this.txtFolderPath.getText();
        if(folderPath.length() == 0) {
            JOptionPane.showMessageDialog(this, "请输入生成位置");
            return false;
        }
        String ip = this.txtIp.getText();
        if(ip.length() == 0) {
            JOptionPane.showMessageDialog(this, "请输入IP");
            return false;
        }
        int port = 0;
        try {
            port = Integer.parseInt(this.txtPort.getText());
        }catch(Exception e) {
            JOptionPane.showMessageDialog(this, "端口设置不正确");
            return false;
        }
        if(port < 1 || port > 65535) {
            JOptionPane.showMessageDialog(this, "端口范围在1~65535");
            return false;
        }
        String username = this.txtUsername.getText();
        if(username.length() == 0) {
            JOptionPane.showMessageDialog(this, "请输入用户名");
            return false;
        }
        String password = this.txtPassword.getText();
        if(password.length() == 0) {
            JOptionPane.showMessageDialog(this, "请输入密码");
            return false;
        }
        return true;
    }
    
    //  开始连接
    private void btnConnectMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnConnectMousePressed
        if(checkInput()) {
            List<String> databases = JdbcUtil.getDatabases(this.cbDbType.getSelectedItem().toString(), 
                    this.txtIp.getText(), Integer.parseInt(this.txtPort.getText()),
                    this.txtUsername.getText(), this.txtPassword.getText());
            if(databases == null) {
                JOptionPane.showMessageDialog(this, "连接失败，请检查输入是否正确");
                this.cbDbName.setEnabled(false);
                this.btnCreate.setEnabled(false);
                return;
            }
            if(databases.isEmpty()) {
                JOptionPane.showMessageDialog(this, "当前数据库服务器没有数据库");
                this.cbDbName.setEnabled(false);
                this.btnCreate.setEnabled(false);
                return;
            }
            this.cbDbName.removeAllItems();
            for(String db : databases) {
                this.cbDbName.addItem(db);
            }
            this.cbDbName.setEnabled(true);
            this.btnCreate.setEnabled(true);
        }else {
            this.cbDbName.setEnabled(false);
            this.btnCreate.setEnabled(false);
        }
    }//GEN-LAST:event_btnConnectMousePressed

    private void btnCancelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancelMousePressed
        this.dispose();
    }//GEN-LAST:event_btnCancelMousePressed

    private void btnChoseFolderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChoseFolderActionPerformed
        String path = null;
        File f = null;
        int flag = 0;
        try{
            flag = fcGenPathChooser.showOpenDialog(null);     
        }catch(HeadlessException head){     
            System.out.println("Open File Dialog ERROR!");    
        }
        if(flag == JFileChooser.APPROVE_OPTION){
            //获得该文件
            f = fcGenPathChooser.getSelectedFile();    
            path=f.getPath();
        }
        if(!StringUtils.isNullOrEmpty(path)) {
            this.txtFolderPath.setText(path);
        }
    }//GEN-LAST:event_btnChoseFolderActionPerformed

    private void txtPackageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPackageActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPackageActionPerformed
    
    private void doClose(int retStatus) {
        returnStatus = retStatus;
        setVisible(false);
        dispose();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnChoseFolder;
    private javax.swing.JButton btnConnect;
    private javax.swing.JButton btnCreate;
    private javax.swing.JComboBox cbDbName;
    private javax.swing.JComboBox cbDbType;
    private javax.swing.JFileChooser fcGenPathChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txtAuthor;
    private javax.swing.JTextField txtFolderPath;
    private javax.swing.JTextField txtIp;
    private javax.swing.JTextField txtPackage;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JTextField txtPort;
    private javax.swing.JTextField txtPrefix;
    private javax.swing.JTextField txtProgramName;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables

    private int returnStatus = RET_CANCEL;
}
