package cn.topcodes.tcsf.codegen.domain.entity;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import java.io.Serializable;

/**
 * 项目
 * @author Unicorn
 */
@DatabaseTable(tableName = "program")  
public class Program implements Serializable {
    
    @DatabaseField(generatedId = true)  
    private Integer id;     //  项目ID
    
    @DatabaseField(columnName= "name", canBeNull = false)
    private String name;    //  项目名称
    
    @DatabaseField(columnName= "package_name", canBeNull = false)
    private String packageName; //  包名
    
    @DatabaseField(columnName= "author", canBeNull = false)
    private String author;      //  作者
    
    @DatabaseField(columnName= "folder_path", canBeNull = false)
    private String folderPath;  //  存储位置
    
    @DatabaseField(columnName= "prefix", canBeNull = true)
    private String prefix;      //  去除前缀
    
    @DatabaseField(columnName= "db_type", canBeNull = false)
    private String dbType;    //  数据库类型
    
    @DatabaseField(columnName= "ip", canBeNull = false)
    private String ip;      //  数据库服务器IP
    
    @DatabaseField(columnName= "port", canBeNull = false)
    private int port;       //  数据库服务器端口
    
    @DatabaseField(columnName= "username", canBeNull = false)
    private String username;    //  数据库用户名
    
    @DatabaseField(columnName= "password", canBeNull = false)
    private String password;    //  数据库密码
    
    @DatabaseField(columnName= "db_name", canBeNull = false)
    private String dbName;      //  数据库名称
    
    @ForeignCollectionField
    private ForeignCollection<Entity> entitys;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public ForeignCollection<Entity> getEntitys() {
        return entitys;
    }

    public void setEntitys(ForeignCollection<Entity> entitys) {
        this.entitys = entitys;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    
    @Override
    public String toString() {
        return name;
    }

}
