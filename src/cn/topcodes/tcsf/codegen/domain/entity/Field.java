package cn.topcodes.tcsf.codegen.domain.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.io.Serializable;

/**
 * 字段
 * @author Unicorn
 */
@DatabaseTable(tableName = "field_info")  
public class Field implements Serializable {
    
    @DatabaseField(generatedId = true)  
    private Integer id;             //  实体ID
    
    @DatabaseField(columnName= "field_name", canBeNull = false)
    private String fieldName;       //  数据库字段名称
    
    @DatabaseField(columnName= "property_name", canBeNull = false)
    private String propertyName;    //  实体属性名称
    
    @DatabaseField(columnName= "display_name", canBeNull = false)
    private String displayName;     //  实体属性显示名称
    
    @DatabaseField(columnName= "db_data_type", canBeNull = false)
    private String dbDataType;        //  数据库数据类型
    
    @DatabaseField(columnName= "jdbc_data_type", canBeNull = false)
    private String jdbcDataType;        //  JDBC数据类型
    
    @DatabaseField(columnName= "allow_null", canBeNull = false)
    private boolean allowNull;     //  允许为空
    
    @DatabaseField(columnName= "is_unique", canBeNull = false)
    private boolean unique;       //  是否唯一
    
    @DatabaseField(columnName= "max", canBeNull = false)
    private Double max;             //  最大值
    
    @DatabaseField(columnName= "min", canBeNull = false)
    private Double min;             //  最小值
    
    @DatabaseField(columnName= "default_value", canBeNull = false)
    private String defaultValue;    //  默认值
    
    @DatabaseField(columnName= "input_type", canBeNull = false)
    private String inputType;    //  输入类型：字符串、整型、浮点型、日期、下拉选择、多选、单选
    
    @DatabaseField(columnName= "is_display_field", canBeNull = false)
    private boolean displayField; //  是否列表显示字段
    
    @DatabaseField(columnName= "is_form_field", canBeNull = false)
    private boolean formField;        //  是否表单字段
    
    @DatabaseField(columnName= "is_query_field", canBeNull = false)
    private boolean queryField;        //  是否查询字段
    
    @DatabaseField(columnName= "query_condition", canBeNull = false)
    private String queryCondition;      //  查询条件
    
    @DatabaseField(foreign = true,foreignAutoRefresh = true)
    private Entity entity;              //  所属实体

    @DatabaseField(columnName= "reg_exp", canBeNull = false)
    private String regExp;          //  正则表达式
    
    @DatabaseField(columnName= "i18n", canBeNull = false)
    private String i18n;            //  国际化
    
    @DatabaseField(columnName= "readonly", canBeNull = false)
    private Boolean readonly;            //  只读
    
    @DatabaseField(columnName= "plain_password", canBeNull = false)
    private Boolean plainPassword;  //  明文密码
    
    @DatabaseField(columnName= "encrypt_password", canBeNull = false)
    private Boolean encryptPassword;    //  密码加密存储
    
    @DatabaseField(columnName= "confirm_password", canBeNull = false)
    private Boolean confirmPassword;    //  生成确认密码
    
    @DatabaseField(columnName= "sub_input_type", canBeNull = false)
    private String subInputType;     //  子输入类型
    
    @DatabaseField(columnName= "select_items", canBeNull = false)
    private String selectItems; //  选项
    
    @DatabaseField(columnName= "current_date", canBeNull = false)
    private Boolean currentDate;       //   以当前时间
    
    @DatabaseField(columnName= "time_limit", canBeNull = false)
    private String timeLimit;       //  时间限制
    
    @DatabaseField(columnName= "time_format", canBeNull = false)
    private String timeFormat;  //  时间格式
    
    @DatabaseField(columnName= "query_data_type", canBeNull = false)
    private String queryDataType;   //  查询数据类型

    public String getQueryDataType() {
        return queryDataType;
    }

    public void setQueryDataType(String queryDataType) {
        this.queryDataType = queryDataType;
    }
    
    public boolean isQueryField() {
        return queryField;
    }

    public void setQueryField(boolean queryField) {
        this.queryField = queryField;
    }

    public String getQueryCondition() {
        return queryCondition;
    }

    public void setQueryCondition(String queryCondition) {
        this.queryCondition = queryCondition;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDbDataType() {
        return dbDataType;
    }

    public void setDbDataType(String dbDataType) {
        this.dbDataType = dbDataType;
    }

    public String getJdbcDataType() {
        return jdbcDataType;
    }

    public void setJdbcDataType(String jdbcDataType) {
        this.jdbcDataType = jdbcDataType;
    }

    public boolean isAllowNull() {
        return allowNull;
    }

    public void setAllowNull(boolean allowNull) {
        this.allowNull = allowNull;
    }

    public boolean isUnique() {
        return unique;
    }

    public void setUnique(boolean unique) {
        this.unique = unique;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public boolean isDisplayField() {
        return displayField;
    }

    public void setDisplayField(boolean displayField) {
        this.displayField = displayField;
    }

    public boolean isFormField() {
        return formField;
    }

    public void setFormField(boolean formField) {
        this.formField = formField;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public String getRegExp() {
        return regExp;
    }

    public void setRegExp(String regExp) {
        this.regExp = regExp;
    }

    public String getI18n() {
        return i18n;
    }

    public void setI18n(String i18n) {
        this.i18n = i18n;
    }

    public Boolean isReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    public Boolean isPlainPassword() {
        return plainPassword;
    }

    public void setPlainPassword(Boolean plainPassword) {
        this.plainPassword = plainPassword;
    }

    public Boolean isEncryptPassword() {
        return encryptPassword;
    }

    public void setEncryptPassword(Boolean encryptPassword) {
        this.encryptPassword = encryptPassword;
    }

    public Boolean isConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(Boolean confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getSubInputType() {
        return subInputType;
    }

    public void setSubInputType(String subInputType) {
        this.subInputType = subInputType;
    }

    public String getSelectItems() {
        return selectItems;
    }

    public void setSelectItems(String selectItems) {
        this.selectItems = selectItems;
    }

    public Boolean isCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Boolean currentDate) {
        this.currentDate = currentDate;
    }

    public String getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit;
    }

    public String getTimeFormat() {
        return timeFormat;
    }

    public void setTimeFormat(String timeFormat) {
        this.timeFormat = timeFormat;
    }
    
    
}
