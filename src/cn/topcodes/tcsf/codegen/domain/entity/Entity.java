package cn.topcodes.tcsf.codegen.domain.entity;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import java.io.Serializable;

/**
 *
 * @author Unicorn
 */
@DatabaseTable(tableName = "entity")  
public class Entity implements Serializable {
    
    @DatabaseField(generatedId = true)  
    private Integer id;     //  实体ID
    
    @DatabaseField(foreign = true,foreignAutoRefresh = true) 
    private Program program;    //  所属项目
    
    @DatabaseField(columnName= "table_name", canBeNull = false)
    private String tableName;   //  表名
    
    @DatabaseField(columnName= "entity_class", canBeNull = false)
    private String entityClass; //  实体类全名
    
    @DatabaseField(columnName= "vo_class", canBeNull = false)
    private String voClass;     //  Vo类全名
    
    @DatabaseField(columnName= "entity_2_vo_convertor_class", canBeNull = false)
    private String entity2VoConvertorClass;     //  实体Vo转换器类全名
    
    @DatabaseField(columnName= "form_class", canBeNull = false)
    private String formClass;           //  表单类全名
    
    @DatabaseField(columnName= "form_2_entity_convertor_class", canBeNull = false)
    private String form2EntityConvertorClass;   //  表单实体转换器类全名
    
    @DatabaseField(columnName= "display_name", canBeNull = false)
    private String displayName;         //  显示名称
    
    @DatabaseField(columnName= "mapper_class", canBeNull = false)
    private String mapperClass;         //  Mapper类全名
    
    @DatabaseField(columnName= "service_class", canBeNull = false)
    private String serviceClass;        //  Service类全名
    
    @DatabaseField(columnName= "controller_class", canBeNull = false)
    private String controllerClass;     //  控制器类全名
    
    @DatabaseField(columnName= "controller_url", canBeNull = false)
    private String controllerUrl;      //   控制器访问路径
    
    @DatabaseField(columnName= "page_folder", canBeNull = false)
    private String pageFolder;        //    JSP存放文件夹
    
    @ForeignCollectionField
    private ForeignCollection<Field> fields;    //  字段列表

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEntityClass() {
        return entityClass;
    }

    public void setEntityClass(String entityClass) {
        this.entityClass = entityClass;
    }

    public String getVoClass() {
        return voClass;
    }

    public void setVoClass(String voClass) {
        this.voClass = voClass;
    }

    public String getEntity2VoConvertorClass() {
        return entity2VoConvertorClass;
    }

    public void setEntity2VoConvertorClass(String entity2VoConvertorClass) {
        this.entity2VoConvertorClass = entity2VoConvertorClass;
    }

    public String getFormClass() {
        return formClass;
    }

    public void setFormClass(String formClass) {
        this.formClass = formClass;
    }

    public String getForm2EntityConvertorClass() {
        return form2EntityConvertorClass;
    }

    public void setForm2EntityConvertorClass(String form2EntityConvertorClass) {
        this.form2EntityConvertorClass = form2EntityConvertorClass;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getMapperClass() {
        return mapperClass;
    }

    public void setMapperClass(String mapperClass) {
        this.mapperClass = mapperClass;
    }

    public String getServiceClass() {
        return serviceClass;
    }

    public void setServiceClass(String serviceClass) {
        this.serviceClass = serviceClass;
    }

    public String getControllerClass() {
        return controllerClass;
    }

    public void setControllerClass(String controllerClass) {
        this.controllerClass = controllerClass;
    }

    public String getControllerUrl() {
        return controllerUrl;
    }

    public void setControllerUrl(String controllerUrl) {
        this.controllerUrl = controllerUrl;
    }

    public String getPageFolder() {
        return pageFolder;
    }

    public void setPageFolder(String pageFolder) {
        this.pageFolder = pageFolder;
    }

    public ForeignCollection<Field> getFields() {
        return fields;
    }

    public void setFields(ForeignCollection<Field> fields) {
        this.fields = fields;
    }

    
}
