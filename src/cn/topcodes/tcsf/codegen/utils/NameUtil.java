/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cn.topcodes.tcsf.codegen.utils;

/**
 *
 * @author 大爱阳哥
 */
public class NameUtil {
    
    public static String underline2Camel(String underline) {
        StringBuilder sb = new StringBuilder();
        String[] words = underline.split("_");
        if(words.length == 0) {
            return underline;
        }
        sb.append(words[0]);
        for(int i = 1; i < words.length ; i ++ ) {
            char[] cs = words[i].toCharArray();
            cs[0]-=32;
            sb.append(String.valueOf(cs));
        }
        return sb.toString();
    }
  
    public static String underline2Pascal(String underline) {
        StringBuilder sb = new StringBuilder();
        String[] words = underline.split("_");
        if(words.length == 0) {
            return underline;
        }
        for(int i = 0; i < words.length ; i ++ ) {
            char[] cs = words[i].toCharArray();
            cs[0]-=32;
            sb.append(String.valueOf(cs));
        }
        return sb.toString();
    }
}
