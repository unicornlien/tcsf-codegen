/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cn.topcodes.tcsf.codegen.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author 大爱阳哥
 */
public class DateUtil {
    
    public static String formatCurrentDate() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }
    
}
