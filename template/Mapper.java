package ${mapperPackage};

import ${entityClass};
import tk.mybatis.mapper.common.Mapper;

/**
 * ${displayName} Mapper
 * @author ${author}
 * @date ${date}
 */
public interface ${mapperName} extends Mapper<${entityName}> {
	
}
