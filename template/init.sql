<#list entitys as entity>
-- ${entity.displayName}
INSERT INTO `t_sys_menu` (`created_time`, `updated_time`, `data_state`, `name`, `url`, `description`, `sort`, `i18n`, `icon_cls`, `parent_id`) VALUES (now(), now(), 'Enable', '${entity.displayName}', '${entity.controllerUrl}', '', '0', null, null, NULL);

INSERT INTO `t_sys_url` (`created_time`, `updated_time`, `data_state`, `url`, `description`) VALUES (now(), now(), 'Enable', '${entity.controllerUrl}/page-list', '${entity.displayName}-获取${entity.displayName}分页数据');
INSERT INTO `t_sys_url` (`created_time`, `updated_time`, `data_state`, `url`, `description`) VALUES (now(), now(), 'Enable', '${entity.controllerUrl}/list', '${entity.displayName}-获取${entity.displayName}列表数据');
INSERT INTO `t_sys_url` (`created_time`, `updated_time`, `data_state`, `url`, `description`) VALUES (now(), now(), 'Enable', '${entity.controllerUrl}/create', '${entity.displayName}-创建${entity.displayName}');
INSERT INTO `t_sys_url` (`created_time`, `updated_time`, `data_state`, `url`, `description`) VALUES (now(), now(), 'Enable', '${entity.controllerUrl}/update', '${entity.displayName}-修改${entity.displayName}');
INSERT INTO `t_sys_url` (`created_time`, `updated_time`, `data_state`, `url`, `description`) VALUES (now(), now(), 'Enable', '${entity.controllerUrl}/careful-delete', '${entity.displayName}-删除${entity.displayName}');
INSERT INTO `t_sys_url` (`created_time`, `updated_time`, `data_state`, `url`, `description`) VALUES (now(), now(), 'Enable', '${entity.controllerUrl}/detail', '${entity.displayName}-查看${entity.displayName}详情');
INSERT INTO `t_sys_url` (`created_time`, `updated_time`, `data_state`, `url`, `description`) VALUES (now(), now(), 'Enable', '${entity.controllerUrl}/exist', '${entity.displayName}-检查${entity.displayName}属性是否存在');

INSERT INTO `t_sys_permission` (`created_time`, `updated_time`, `data_state`, `name`, `description`, `code`, `parent_id`, `sort`) select now(), now(), 'Enable', '${entity.displayName}管理权限', '${entity.displayName}增删改查', '${entity.displayName}', id, '1' from t_sys_permission where code = 'Other';
INSERT INTO `t_sys_menu_permission_rela` (`created_time`, `updated_time`, `data_state`, `perm_id`, `menu_id`) select now(), now(), 'Enable', p.id, m.id from t_sys_permission p, t_sys_menu m where p.name = '${entity.displayName}管理权限' and m.url='${entity.controllerUrl}';
INSERT INTO `t_sys_url_permission_rela` (`created_time`, `updated_time`, `data_state`, `perm_id`, `url_id`) select now(), now(), 'Enable', p.id, u.id from t_sys_permission p, t_sys_url u where p.name = '${entity.displayName}管理权限' and u.url = '${entity.controllerUrl}/page-list';
INSERT INTO `t_sys_url_permission_rela` (`created_time`, `updated_time`, `data_state`, `perm_id`, `url_id`) select now(), now(), 'Enable', p.id, u.id from t_sys_permission p, t_sys_url u where p.name = '${entity.displayName}管理权限' and u.url = '${entity.controllerUrl}/list';
INSERT INTO `t_sys_url_permission_rela` (`created_time`, `updated_time`, `data_state`, `perm_id`, `url_id`) select now(), now(), 'Enable', p.id, u.id from t_sys_permission p, t_sys_url u where p.name = '${entity.displayName}管理权限' and u.url = '${entity.controllerUrl}/create';
INSERT INTO `t_sys_url_permission_rela` (`created_time`, `updated_time`, `data_state`, `perm_id`, `url_id`) select now(), now(), 'Enable', p.id, u.id from t_sys_permission p, t_sys_url u where p.name = '${entity.displayName}管理权限' and u.url = '${entity.controllerUrl}/update';
INSERT INTO `t_sys_url_permission_rela` (`created_time`, `updated_time`, `data_state`, `perm_id`, `url_id`) select now(), now(), 'Enable', p.id, u.id from t_sys_permission p, t_sys_url u where p.name = '${entity.displayName}管理权限' and u.url = '${entity.controllerUrl}/careful-delete';
INSERT INTO `t_sys_url_permission_rela` (`created_time`, `updated_time`, `data_state`, `perm_id`, `url_id`) select now(), now(), 'Enable', p.id, u.id from t_sys_permission p, t_sys_url u where p.name = '${entity.displayName}管理权限' and u.url = '${entity.controllerUrl}/careful-detail';
INSERT INTO `t_sys_url_permission_rela` (`created_time`, `updated_time`, `data_state`, `perm_id`, `url_id`) select now(), now(), 'Enable', p.id, u.id from t_sys_permission p, t_sys_url u where p.name = '${entity.displayName}管理权限' and u.url = '${entity.controllerUrl}/careful-exist';

</#list> 


<#list entitys as entity>
-- ${entity.displayName}
delete from `t_sys_menu` where name = '${entity.displayName}' and url = '${entity.controllerUrl}';

delete from `t_sys_url` where url = '${entity.controllerUrl}/page-list';
delete from `t_sys_url` where url = '${entity.controllerUrl}/list';
delete from `t_sys_url` where url = '${entity.controllerUrl}/create';
delete from `t_sys_url` where url = '${entity.controllerUrl}/update';
delete from `t_sys_url` where url = '${entity.controllerUrl}/careful-delete';
delete from `t_sys_url` where url = '${entity.controllerUrl}/detail';
delete from `t_sys_url` where url = '${entity.controllerUrl}/exist';

delete from t_sys_permission where name = '${entity.displayName}管理权限';

</#list> 
