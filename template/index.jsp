<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/layouts/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>${displayName}</title>
	<%@include file="/WEB-INF/layouts/index-header.jsp"%>
</head>
<body>

<div class="tpanel">
	<div class="panel-content">
		<div class="container-fluid">
			<div id="tool">
				<form id="searchForm" class="form-inline" onsubmit="return search('#searchForm','#table')" style="margin-top:20px;margin-bottom:20px;">
					<#list fields as field>
					<#if field.queryField>
					<div class="form-group">
						<label class="">${field.displayName}：</label>
						<input type="text" name="search_${field.queryCondition}${field.queryDataType}_${field.propertyName}" class="form-control"/>
					</div>
					</#if>
					</#list>
					<div class="form-group">
						<#if hasQueryField>
						<button type="submit" class="btn btn-primary radius">
							<span class="glyphicon glyphicon-search"></span> 查询
						</button>
						</#if>
						<a class="btn btn-success radius" href="${r'${ctx}'}${controllerUrl}/create">
							<span class="glyphicon glyphicon-cog"></span> 创建
						</a>
					</div>
				</form>
			</div>
			<table id="table"></table>
		</div>
	</div>
</div>

<script type="text/javascript">
    changeTitle('${displayName}');

    $(function() {
        $('#table').bootstrapTable({
            url: '${r'${ctx}'}${controllerUrl}/page-list',
            idField : 'id',
            striped : true,
            pagination : true,
            pageNumber : 1,
            pageSize : 25,
            sidePagination : 'server',
            uniqueId : 'id',
            /*checkbox : true,
            clickToSelect : true,*/
            queryParams : queryParams,
            responseHandler : responseHandler,
            columns: [/*{
                field: 'ck',
                checkbox : true
            },*/
			{
				title: '#',
				width : 50,
				align : 'center',
				formatter: snFormatter
			},
			<#list fields as field>
			<#if field.displayField>
			{
				field: '${field.propertyName}',
				title: '${field.displayName}',
				align : 'center'
			},
			</#if>
			</#list>  
			{
                field: 'op',
                title: '操作',
                align : 'center',
                formatter : operateFormatter
            }]
        });

    });

    function operateFormatter(value,row,index) {
		var content = '<a title="查看详情" href="javascript:detail(' + row.id + ')"><i class="iconfont icon-caidanguanli"></i></a>';
        content += '&nbsp;&nbsp;&nbsp;&nbsp;';
        content += '<a href="${r'${ctx}'}${controllerUrl}/update?id=' + row.id + '" title="编辑"><i class="iconfont icon-iconfontcolor32"></i></a>';
        content += '&nbsp;&nbsp;&nbsp;&nbsp;';
        content += '<a href="javascript:del(' + row.id + ')" title="删除"><i class="iconfont icon-shanchu"></i></a>';
        return content;
    }
    function detail(id) {
        openWindow('查看详情','${r'${ctx}'}${controllerUrl}/detail?id=' + id);
    }
    function del(id,name) {
        carefulDelete('${r'${ctx}'}${controllerUrl}/careful-delete',id,'请输入您的管理密码以确定删除此系统资源');
    }
</script>

</body>
</html>
