package ${entity2VoConvertorPackage};

import ${entityClass};
import ${voClass};
import cn.topcodes.tcsf.admin.commons.convertor.Entity2VoConvertor;
import org.springframework.stereotype.Component;

/**
 * ${displayName} 实体->表单转换器
 * @author ${author}
 * @date ${date}
 */
@Component
public class ${entity2VoConvertorName} extends Entity2VoConvertor<${entityName},${voName}> {

}
