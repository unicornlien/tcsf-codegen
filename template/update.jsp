<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/layouts/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>编辑${displayName}</title>
    <%@include file="/WEB-INF/layouts/edit-header.jsp"%>
</head>
<body>
    <div class="tpanel">
        <div class="panel-content">
            <div class="container-fluid">

                <form id="createForm" class="form-horizontal">
                    <input type="hidden" name="id" value="${r'${entity.id}'}" />
                    <#list fields as field>  
                        <#if field.formField>

                        <#if field.inputType == "密码">
                        <div class="form-group<#if field_index == 0> form-group-first</#if>">
                            <label for="${field.propertyName}" class="col-sm-2 control-label">${field.displayName}</label>
                            <div class="col-sm-8">
                            <input class="form-control" 
                            <#if field.isPlainPassword()>
                                    type="text"
                            <#else>
                                    type="password"
                            </#if> 
                                    id="${field.propertyName}" name="${field.propertyName}" 
                            <#if field.allowNull>
                                    placeholder="请输入${field.displayName}" 
                            <#else>  
                                    placeholder="请输入${field.displayName}(必填)" required 
                            </#if> 
                                    minlength="${field.min}" maxlength="${field.max}" 
                                    />
                            </div>
                        </div>
                        <#if field.isConfirmPassword()>
                        <div class="form-group">
                            <label for="${field.propertyName}Confirm" class="col-sm-2 control-label">确认${field.displayName}</label>
                            <div class="col-sm-8">
                            <input class="form-control" 
                            <#if field.isPlainPassword()>
                                    type="text"
                            <#else>
                                    type="password"
                            </#if> 
                                    id="${field.propertyName}Confirm" name="${field.propertyName}Confirm" 
                                    placeholder="请再次输入${field.displayName}" 
                                    equalTo="#${field.propertyName}"
                                    />
                            </div>
                        </div>
                        </#if>
                        <#else>
                        <div class="form-group<#if field_index == 0> form-group-first</#if>">
                            <label for="${field.propertyName}" class="col-sm-2 control-label">${field.displayName}</label>
                            <div class="col-sm-8">
                            <#if field.jdbcDataType == "Date">
                                    <input type="text" class="form-control tcsf-datetimepicker"  <#if field.isReadonly()>readonly</#if>  
                                            id="${field.propertyName}" name="${field.propertyName}" 
                                            value='<fmt:formatDate value="${r'${entity.'}${field.propertyName}}" pattern="${field.timeFormat}" />' 
                                    <#if field.allowNull>
                                            placeholder="请输入${field.displayName}" 
                                    <#else>  
                                            placeholder="请输入${field.displayName}(必填)" required 
                                    </#if> 
                                            />
                            </#if>
                            <#if field.inputType == "文件上传">
                                <input class="form-control" type="file" 
                                        id="${field.propertyName}" name="${field.propertyName}" 
                                <#if field.allowNull>
                                        placeholder="请上传文件${field.displayName}" 
                                <#else>  
                                        placeholder="请上传文件${field.displayName}(必需)" required 
                                </#if> 
                                        />
                            </#if>
                            <#if field.inputType == "富文本">
                                <textarea class="form-control" 
                                        id="${field.propertyName}" name="${field.propertyName}" 
                                <#if field.allowNull>
                                        placeholder="请输入${field.displayName}" 
                                <#else>  
                                        placeholder="请输入${field.displayName}(必填)" required 
                                </#if> 
                                        >
                                </textarea>
                            </#if>
                            <#if field.inputType == "文本">
                                <input type="text" class="form-control" <#if field.isReadonly()>readonly</#if> 
                                    id="${field.propertyName}" name="${field.propertyName}" 
                                    value="${r'${entity.'}${field.propertyName}}" 
                                    <#if field.subInputType == "邮箱">
                                    email="true"
                                    </#if>
                                    <#if field.subInputType == "手机">
                                    isMobile="true"
                                    </#if>
                                    <#if field.subInputType == "身份证">
                                    isIdCardNo="true"
                                    </#if>
                                    <#if field.regExp != "">
                                    ${field.propertyName}Valid="true"
                                    </#if>
                                    <#if field.allowNull>
                                            placeholder="请输入${field.displayName}" 
                                    <#else>  
                                            placeholder="请输入${field.displayName}(必填)" required 
                                    </#if> 
                                    minlength="${field.min}" maxlength="${field.max}" 
                                    <#if field.unique>
                                            remote="${r'${ctx}'}${controllerUrl}/exist?id=${r'${entity.id}'}&property=${field.propertyName}" 
                                    </#if> 
                                    />
                            </#if>
                                <#if field.inputType == "数值">
                                    <input type="number" class="form-control"  <#if field.isReadonly()>readonly</#if> 
                                            id="${field.propertyName}" name="${field.propertyName}" 
                                            value="${r'${entity.'}${field.propertyName}}" 
                                            <#if field.subInputType == "小数">
                                            number="true"
                                            <#elseif field.subInputType == "整数">
                                            digits="true"
                                            </#if>
                                            <#if field.allowNull>
                                                    placeholder="请输入${field.displayName}" 
                                            <#else>  
                                                    placeholder="请输入${field.displayName}(必填)" required 
                                            </#if> 
                                            min="${field.min}" max="${field.max}" 
                                            />
                                </#if>
                                </div>
                        </div>
                        </#if>
                                </#if>
                    </#list>

                    <div class="form-group">
                        <div class="col-sm-12" style="text-align: center;">
                                <a class="btn btn-default" href="javascript:back();">返回</a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="submit" class="btn btn-primary" value="保存" />
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
                                                                
    <script type="text/javascript">
    changeTitle('编辑${displayName}');
    $(function() {
        <#list fields as field>
        <#if field.regExp != "">
        jQuery.validator.addMethod("${field.propertyName}Valid", function(value, element) { 
          var tel = ${field.regExp}; 
          return this.optional(element) || (tel.test(value)); 
        }, "您的输入不正确"); 
        </#if>
        </#list>

        initForm('#createForm','${r'${ctx}'}${controllerUrl}/update',function(r) {
            var resp = $.parseJSON(r);
            if(resp.code == 'OK') {
                changeUrl('${r'${ctx}'}${controllerUrl}');
                topLayer.msg('保存成功', {icon: 1});
            }else {
                topLayer.msg(resp.message, {icon: 2});
            }
        });

    });
    </script>
</body>
</html>
