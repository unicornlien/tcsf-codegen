package ${servicePackage};

import ${entityClass};
import cn.topcodes.tcsf.admin.commons.generic.GenericService;
import org.springframework.stereotype.Service;

/**
 * ${displayName} 服务
 * @author ${author}
 * @date ${date}
 */
@Service
public class ${serviceName} extends GenericService<${entityName}> {

}
