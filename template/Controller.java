package ${controllerPackage};

import ${entityClass};
import ${voClass};
import ${formClass};
import cn.topcodes.tcsf.admin.commons.api.controller.admin.AdminController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ${displayName}管理控制器
 * @author ${author}
 * @date ${date}
 */
@Controller
@RequestMapping("${controllerUrl}")
public class ${controllerName} extends AdminController<${entityName},${voName},${formName}> {

	@Override
	protected String getPageFolder() {
		return "${pageFolder}";
	}

}
